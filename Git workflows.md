|Feature                   |Trac-based | Github | Gerrit |
| --- | --- | --- | --- |
|1-click merge          |No              | Yes     | Yes    |
|Clean history           |Yes             | No      | Yes    |
|Line by line review  |No              |Yes      |Yes     |
|Auto-merge/rebase |No             |Yes      |Yes     |
|Buildbot integration |No              |Only status          |Yes     |
|Developer votes   |No              |?          |Yes     |
|Git integration         |No              |No        |Yes     |